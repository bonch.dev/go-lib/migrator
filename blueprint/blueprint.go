package blueprint

import (
	"context"
	"database/sql"
	"fmt"
	"sort"
	"strings"

	"gitlab.com/bonch.dev/go-lib/migrator/column"
	"gitlab.com/bonch.dev/go-lib/migrator/command"
	"gitlab.com/bonch.dev/go-lib/migrator/grammar"
	"gitlab.com/bonch.dev/go-lib/migrator/helper"
)

// Blueprint is a schema, which used for creating/changing/updating tables
// Blueprint contains commands, that executed in some order, defined in sortCommands() function
type Blueprint struct {
	temporary bool
	table     string
	commands  []command.Command
	columns   []*column.Column
	tx        *sql.Tx
	logWriter helper.Logger
}

// Constructor of new instance of Blueprint
func NewBlueprint(writer helper.Logger, tx *sql.Tx, table string, callback func(blueprint *Blueprint)) *Blueprint {
	b := &Blueprint{table: table, tx: tx, logWriter: writer}

	callback(b)

	return b
}

// GetTable return current operated table
func (b *Blueprint) GetTable() string {
	return b.table
}

// Create is an internal command, which provides creating all columns in DB from Blueprint columns field
func (b *Blueprint) Create() {
	b.commands = append([]command.Command{&command.Default{InnerType: command.CreateCommand}}, b.commands...)
}

// isCreating checks, that Blueprint is in "Creating" state.
// Used for collapse creating command in a single query
func (b *Blueprint) isCreating() bool {
	for _, c := range b.commands {
		if c.GetInnerType() == command.CreateCommand {
			return true
		}
	}

	return false
}

// Build exec all generated SQL commands in given context
// Probably can work in Transaction, if b.tx is setup
func (b *Blueprint) Build(db helper.DB, g grammar.Grammar) {
	b.selectImpliedCommand()
	b.setIndexCommands()

	for _, s := range b.toSQL(g) {
		if s == "" {
			continue
		}

		b.logWriter.Debugf("execute statement: %s", s)

		var err error
		if b.tx != nil {
			_, err = b.tx.ExecContext(context.Background(), s)
		} else {
			_, err = db.ExecContext(context.Background(), s)
		}

		if err != nil {
			panic(err.Error())
		}
	}
}

// toSQL transform all current commands to SQL queries (statements)
func (b *Blueprint) toSQL(g grammar.Grammar) []string {
	var statements []string

	b.sortCommands()

	for _, c := range b.commands {
		switch c.(type) {
		default:
			statements = append(statements, b.defaultToSQL(g, c))
		case *command.Index:
			statements = append(statements, b.indexToSQL(g, c))
		case *command.Foreign:
			statements = append(statements, b.foreignToSQL(g, c))
		case *command.Drop:
			statements = append(statements, b.dropToSQL(g, c))
		}
	}

	return statements
}

// defaultToSQL use Default (Add/Create/Change/Rename) commands to provide SQL statements
func (b *Blueprint) defaultToSQL(g grammar.Grammar, c command.Command) string {
	switch c.GetInnerType() {
	case command.AddCommand:
		return g.CompileAdd(b.GetTable(), b.GetAddedColumns())
	case command.CreateCommand:
		return g.CompileCreate(b.GetTable(), b.temporary, b.GetAddedColumns())
	case command.ChangeCommand, command.RenameCommand, command.RenameIndexCommand:
		panic("[WARNING] change command not implemented")
	}

	return ""
}

// indexToSQL use Index (Primary/Unique/Index) commands to create indexes in a table
func (b *Blueprint) indexToSQL(g grammar.Grammar, c command.Command) string {
	indexCommand, ok := c.(*command.Index)
	if !ok {
		return ""
	}

	switch indexCommand.GetInnerType() {
	case command.PrimaryCommand:
		return g.CompilePrimary(b.GetTable(), indexCommand.GetName(), indexCommand.GetColumns())
	case command.UniqueCommand:
		return g.CompileUnique(b.GetTable(), indexCommand.GetName(), indexCommand.GetColumns())
	case command.IndexCommand:
		return g.CompileIndex(b.GetTable(), indexCommand.GetName(), indexCommand.GetColumns())
	}

	return ""
}

// foreignToSQL use Foreign () commands to create foreign indexes in a table
func (b *Blueprint) foreignToSQL(g grammar.Grammar, c command.Command) string {
	foreignCommand, ok := c.(*command.Foreign)
	if !ok {
		return ""
	}

	return g.CompileForeign(b.GetTable(), foreignCommand)
}

// dropToSQL use Drop () commands to drop some columns or indexes in a table
func (b *Blueprint) dropToSQL(g grammar.Grammar, c command.Command) string {
	drop, ok := c.(*command.Drop)
	if !ok {
		return ""
	}

	switch c.GetInnerType() {
	case command.DropColumnCommand:
		return g.CompileDropColumn(b.GetTable(), drop)
	case command.DropPrimaryCommand:
		return g.CompileDropPrimary(b.GetTable(), drop)
	case command.DropIndexCommand:
		return g.CompileDropIndex(b.GetTable(), drop)
	case command.DropUniqueCommand:
		return g.CompileDropUnique(b.GetTable(), drop)
	case command.DropForeignCommand:
		return g.CompileDropForeign(b.GetTable(), drop)
	}

	return ""
}

// GetAddedColumns return slice of current added columns without columns, which is in a changing state
func (b *Blueprint) GetAddedColumns() []*column.Column {
	var addedColumns []*column.Column

	for _, c := range b.columns {
		if !c.Changed() {
			addedColumns = append(addedColumns, c)
		}
	}

	return addedColumns
}

// GetChangedColumns return slice of current changed columns
func (b *Blueprint) GetChangedColumns() []*column.Column {
	var changedColumns []*column.Column

	for _, c := range b.columns {
		if c.Changed() {
			changedColumns = append(changedColumns, c)
		}
	}

	return changedColumns
}

// AddColumn
func (b *Blueprint) AddColumn(t column.Type, name string) *column.Column {
	c := &column.Column{Type: t, Name: name}

	switch t {

	}

	b.columns = append(b.columns, c)

	return c
}

// DropColumn
func (b *Blueprint) DropColumn(columnName ...string) {
	com := &command.Drop{InnerType: command.DropColumnCommand, Columns: columnName}

	b.commands = append(b.commands, com)
}

// GetColumns return slice of columns in current Blueprint.
// It doesn't return a columns, that already created in selected table
func (b *Blueprint) GetColumns() []*column.Column {
	return b.columns
}

// AddForeign
func (b *Blueprint) AddForeign(columns ...string) *command.Foreign {
	name := b.createIndexName("fk", strings.Join(columns, "_"))

	foreign := &command.Foreign{InnerType: command.ForeignCommand, Name: name, Columns: columns}

	b.commands = append(b.commands, foreign)

	return foreign
}

func (b *Blueprint) AddIndex(columnNames ...string) *command.Index {
	index := b.createIndexName(command.IndexCommand, strings.Join(columnNames, "_"))

	com := &command.Index{
		InnerType: command.IndexCommand,
		Name:      index,
		Columns:   columnNames,
	}

	b.commands = append(b.commands, com)

	return com
}

func (b *Blueprint) AddUnique(columnNames ...string) *command.Index {
	index := b.createIndexName(command.UniqueCommand, strings.Join(columnNames, "_"))

	com := &command.Index{
		InnerType: command.UniqueCommand,
		Name:      index,
		Columns:   columnNames,
	}

	b.commands = append(b.commands, com)

	return com
}

func (b *Blueprint) AddPrimary(columnNames ...string) *command.Index {
	index := b.createIndexName(command.PrimaryCommand, strings.Join(columnNames, "_"))

	com := &command.Index{
		InnerType: command.PrimaryCommand,
		Name:      index,
		Columns:   columnNames,
	}

	b.commands = append(b.commands, com)

	return com
}

// DropIndex
func (b *Blueprint) DropIndex(indexName string) *command.Drop {
	drop := &command.Drop{InnerType: command.DropIndexCommand, Columns: []string{indexName}}

	b.commands = append(b.commands, drop)

	return drop
}

// DropUnique
func (b *Blueprint) DropUnique(indexName string) *command.Drop {
	drop := &command.Drop{InnerType: command.DropUniqueCommand, Columns: []string{indexName}}

	b.commands = append(b.commands, drop)

	return drop
}

// DropPrimary
func (b *Blueprint) DropPrimary() *command.Drop {
	drop := &command.Drop{InnerType: command.DropPrimaryCommand}

	b.commands = append(b.commands, drop)

	return drop
}

// selectImpliedCommand set strategy of creating or updating table.
// If Blueprint is in creating state, it will persisted
func (b *Blueprint) selectImpliedCommand() {
	if len(b.GetAddedColumns()) > 0 && !b.isCreating() {
		b.useAddCommand()
	}

	if len(b.GetChangedColumns()) > 0 && !b.isCreating() {
		b.useChangeCommand()
	}
}

// useAddCommand set strategy of update table with adding some columns
func (b *Blueprint) useAddCommand() {
	b.commands = append([]command.Command{&command.Default{InnerType: command.AddCommand}}, b.commands...)
}

// useChagneCommand set strategy of update table with changing some columns
func (b *Blueprint) useChangeCommand() {
	b.commands = append([]command.Command{&command.Default{InnerType: command.ChangeCommand}}, b.commands...)
}

// setIndexCommands remove index field from Column, and setup index commands
func (b *Blueprint) setIndexCommands() {
	for _, c := range b.GetColumns() {
		for _, index := range []command.TypeName{
			command.PrimaryCommand,
			command.UniqueCommand,
			command.IndexCommand,
		} {
			if c.HasIndex(string(index)) {
				b.useIndexCommand(index, c.Name)
				// c.SetIndex(string(index), false)
			}
		}
	}
}

// useIndexCommand adds Index command
func (b *Blueprint) useIndexCommand(indexType command.TypeName, columnName ...string) {
	index := b.createIndexName(indexType, strings.Join(columnName, "_"))

	com := &command.Index{InnerType: indexType, Name: index, Columns: columnName}

	b.commands = append(b.commands, com)
}

// createIndexName creates formatted index name from table name, used column name and type of index
func (b *Blueprint) createIndexName(indexType command.TypeName, columnName string) string {
	return strings.ReplaceAll(
		strings.ReplaceAll(
			fmt.Sprintf("%s_%s_%s", b.table, columnName, indexType),
			"-", "_"),
		".", "_")
}

// Sorting commands with internal command level logic.
// Add/Change/Create commands are first
// Index commands are second
// Rename commands are third
// Drop commands are last
func (b *Blueprint) sortCommands() {
	sort.Slice(b.commands, func(i, j int) bool {
		ic := b.commands[i]
		jc := b.commands[j]

		return command.SortLevel[ic.GetInnerType()] < command.SortLevel[jc.GetInnerType()]
	})
}
