package blueprint

import "gitlab.com/bonch.dev/go-lib/migrator/column"

func (b *Blueprint) String(name string, length int) *column.Column {
	c := b.AddColumn(column.String, name)

	c.Length(length)

	return c
}
