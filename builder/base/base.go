package base

import (
	"context"
	"database/sql"

	"gitlab.com/bonch.dev/go-lib/migrator/blueprint"
	"gitlab.com/bonch.dev/go-lib/migrator/grammar"
	"gitlab.com/bonch.dev/go-lib/migrator/helper"
)

type Builder struct {
	Grammar       grammar.Grammar
	db            helper.DB
	Tx            *sql.Tx
	LogWriter     helper.Logger
	runPersistent bool
}

func NewBuilder(g grammar.Grammar, db helper.DB, logWriter helper.Logger) *Builder {
	return &Builder{Grammar: g, db: db, LogWriter: logWriter}
}

func (b *Builder) Create(tableName string, callback func(bl *blueprint.Blueprint)) {
	b.Build(blueprint.NewBlueprint(b.LogWriter, b.Tx, tableName, func(bl *blueprint.Blueprint) {
		bl.Create()

		callback(bl)
	}))
}

func (b *Builder) Table(tableName string, callback func(bl *blueprint.Blueprint)) {
	b.Build(blueprint.NewBlueprint(b.LogWriter, b.Tx, tableName, callback))
}

func (b *Builder) Build(bl *blueprint.Blueprint) {
	bl.Build(b.db, b.Grammar)
}

func (b *Builder) SetTransaction(tx *sql.Tx) {
	b.Tx = tx
}

func (b *Builder) DB() helper.DB {
	return b.db
}

func (b *Builder) Connection(ctx context.Context) (*sql.Conn, error) {
	return b.db.Conn(ctx)
}

func (b *Builder) Logger() helper.Logger {
	return b.LogWriter
}

func (b *Builder) CanRunPersistent(r bool) {
	b.runPersistent = r
}

func (b *Builder) Persistent(fn func()) {
	if b.runPersistent {
		fn()
	}
}
