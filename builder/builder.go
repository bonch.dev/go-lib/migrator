package builder

import (
	"context"
	"database/sql"

	"gitlab.com/bonch.dev/go-lib/migrator/blueprint"
	"gitlab.com/bonch.dev/go-lib/migrator/builder/base"
	"gitlab.com/bonch.dev/go-lib/migrator/builder/postgres"
	"gitlab.com/bonch.dev/go-lib/migrator/builder/sqlite"
	"gitlab.com/bonch.dev/go-lib/migrator/grammar"
	postgresGrammar "gitlab.com/bonch.dev/go-lib/migrator/grammar/postgres"
	sqliteGrammar "gitlab.com/bonch.dev/go-lib/migrator/grammar/sqlite"
	"gitlab.com/bonch.dev/go-lib/migrator/helper"
)

type Builder interface {
	DB() helper.DB
	Connection(ctx context.Context) (*sql.Conn, error)

	HasTable(ctx context.Context, tableName string) (bool, error)

	Create(tableName string, callback func(blueprint *blueprint.Blueprint))
	Table(tableName string, callback func(blueprint *blueprint.Blueprint))
	Build(blueprint *blueprint.Blueprint)
	Exec(command string) error
	DropTable(tableName string) error
	DropTableIfExists(tableName string) error
	Wipe() error

	// BatchSQL return SQL, that select current batch counter.
	// If no migrations, Batch Counter must be 1
	// In another case, Batch Counter must be max(batch) + 1
	BatchSQL() string

	SetTransaction(tx *sql.Tx)

	Persistent(fn func())
	CanRunPersistent(ability bool)
}

func NewBuilder(db helper.DB, g grammar.Grammar, logger helper.Logger) Builder {
	baseBuilder := *base.NewBuilder(g, db, logger)

	switch g.(type) {
	case *postgresGrammar.SQLGrammar:
		return &postgres.Builder{Builder: baseBuilder}
	case *sqliteGrammar.SQLGrammar:
		return &sqlite.Builder{Builder: baseBuilder}

	// another Grammars implementation
	// case *mysqlGrammar.SQLGrammar:
	//     return &mysql.BaseBuilder {
	//         Grammar: grammar,
	//         Conn: connection,
	//     }

	default:
		panic("builder not defined")
	}
}
