package postgres

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/bonch.dev/go-lib/migrator/builder/base"
	"gitlab.com/bonch.dev/go-lib/migrator/grammar/postgres"
)

type Builder struct {
	base.Builder
}

func (b *Builder) HasTable(ctx context.Context, tableName string) (bool, error) {
	rows, err := b.DB().QueryContext(ctx,
		b.Grammar.CompileTableExists("public", tableName),
	)

	if err != nil {
		return false, err
	}

	if err := rows.Err(); err != nil {
		return false, err
	}

	defer rows.Close()

	return rows.Next(), nil
}

func (b *Builder) DropTableIfExists(tableName string) error {
	stmt := fmt.Sprintf("drop table if exists %s", postgres.WrapValue(tableName))

	b.LogWriter.Debugf("execute statement: %s", stmt)

	_, err := b.DB().ExecContext(
		context.Background(),
		stmt,
	)

	if err != nil {
		return err
	}

	return nil
}

func (b *Builder) DropTable(tableName string) error {
	stmt := fmt.Sprintf("drop table %s", postgres.WrapValue(tableName))

	b.LogWriter.Debugf("execute statement: %s", stmt)

	_, err := b.DB().ExecContext(
		context.Background(),
		stmt,
	)

	if err != nil {
		return err
	}

	return nil
}

func (b *Builder) Wipe() error {
	stmt := "select tablename from pg_catalog.pg_tables where schemaname in ('public')"

	b.LogWriter.Debugf("execute statement: %s", stmt)

	rows, err := b.DB().Query(stmt)
	if err != nil {
		return err
	}

	if err := rows.Err(); err != nil {
		return err
	}

	defer rows.Close()

	tables := make([]string, 0)

	for rows.Next() {
		var tablename string

		if err := rows.Scan(&tablename); err != nil {
			return err
		}

		tables = append(tables, tablename)
	}

	if len(tables) == 0 {
		b.LogWriter.Infof("No tables found, skipping freshing.")
		return nil
	}

	stmt = fmt.Sprintf("drop table \"%s\" cascade", strings.Join(tables, "\",\""))

	b.LogWriter.Debugf("execute statement: %s", stmt)

	_, err = b.DB().ExecContext(
		context.Background(),
		stmt,
	)

	if err != nil {
		return err
	}

	return nil
}

func (b *Builder) Exec(command string) error {
	b.LogWriter.Debugf("executing command: %s", command)

	_, err := b.DB().ExecContext(
		context.Background(),
		command,
	)

	return err
}

func (Builder) BatchSQL() string {
	return "select coalesce(max(batch), 0) + 1 as batch from migrations"
}
