package cmd

import (
	_ "embed" // embed template file
	"fmt"
	"os"
	"strings"
	"text/template"
	"time"
	"unicode/utf8"

	"github.com/iancoleman/strcase"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
)

var (
	//go:embed migration.go.tpl
	createTpl string
)

var (
	ErrNoMigrationName = errors.New("no migration name present")
)

func CreateCommand() *cobra.Command {
	createCommand := &cobra.Command{
		Use:                   "create [-d destination] [-p package] migration name",
		DisableFlagsInUseLine: true,
		Short:                 "Create migration",
		RunE: func(cmd *cobra.Command, args []string) error {
			if len(args) == 0 {
				return ErrNoMigrationName
			}

			destination, err := cmd.Flags().GetString("destination")
			if err != nil {
				return errors.Wrap(err, "destination not set")
			}

			pack, err := cmd.Flags().GetString("package")
			if err != nil {
				return errors.Wrap(err, "package not set")
			}

			dateF := time.Now().Format("2006_01_02_150405")
			dateT := time.Now().Format("20060102150405")

			destinationPath := parsePathFromDestination(destination)

			if pack == "" {
				if destination != "." {
					pack = destinationPath[len(destinationPath)-1]
				} else {
					pack = "main"
				}
			}

			path := formatPath(
				strings.Join(destinationPath, "/"),
				strcase.ToSnake(strings.Join(args, "_")),
				dateF,
			)

			file, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE, 0755) //nolint:gomnd - 0755 default permission for code files
			if err != nil {
				return err
			}
			defer func() {
				if err := file.Close(); err != nil {
					panic(err)
				}
			}()

			err = template.
				Must(template.New("migration").Parse(createTpl)).
				Execute(file, struct {
					Package       string
					Date          string
					MigrationName string
				}{
					Package:       pack,
					MigrationName: strcase.ToCamel(strings.Join(args, "-")),
					Date:          dateT,
				})
			if err != nil {
				return err
			}

			fmt.Printf("Succesfully created migration - %s\n", path)

			return nil
		},
	}

	createCommand.Flags().StringP("destination", "d", ".", "Destination of creating migration")
	createCommand.Flags().StringP("package", "p", "", "Package name of template")

	return createCommand
}

func parsePathFromDestination(destination string) []string {
	exploded := strings.Split(destination, "/")
	path := make([]string, 0)

	for _, s := range exploded {
		if utf8.RuneCountInString(s) > 0 {
			path = append(path, s)
		}
	}

	return path
}

func formatPath(destination, migrationNameSnake, dateF string) string {
	return fmt.Sprintf("%s/%s_%s.go", destination, dateF, migrationNameSnake)
}
