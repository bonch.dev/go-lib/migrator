package {{ .Package }}

import (
	"gitlab.com/bonch.dev/go-lib/migrator/builder"
	"gitlab.com/bonch.dev/go-lib/migrator/migration"
	"gitlab.com/bonch.dev/go-lib/migrator/migrator"
)

func init() {
	migrator.AddMigrations(&M{{ .Date }}{{ .MigrationName }}{})
}

type M{{ .Date }}{{ .MigrationName }} struct {
	migration.BaseMigration
}

func (m M{{ .Date }}{{ .MigrationName }}) Up(b builder.Builder) {

}

func (m M{{ .Date }}{{ .MigrationName }}) Down(b builder.Builder) {

}

func (m M{{ .Date}}{{ .MigrationName }}) Name() string {
	return "M{{ .Date}}{{ .MigrationName }}"
}