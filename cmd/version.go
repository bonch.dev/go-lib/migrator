package cmd

import (
	"os"
	"runtime/debug"

	"github.com/spf13/cobra"
)

var VersionCommand = &cobra.Command{
	Use:   "version",
	Short: "Show version of application",
	Run: func(cmd *cobra.Command, args []string) {
		i, ok := debug.ReadBuildInfo()
		if !ok {
			println("(devel) (~crash~)")

			os.Exit(1)
		}

		println(i.Main.Version)
	},
}
