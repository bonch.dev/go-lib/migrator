package column

type Type string

const (
	Char          Type = "char"
	String        Type = "string"
	Text          Type = "text"
	MediumText    Type = "medium_text"
	LongText      Type = "long_text"
	Integer       Type = "integer"
	TinyInteger   Type = "tinyint"
	SmallInteger  Type = "smallint"
	MediumInteger Type = "mediumint"
	BigInteger    Type = "bigint"
	Double        Type = "double"
	Real          Type = "real"
	Decimal       Type = "decimal"
	Boolean       Type = "boolean"
	JSON          Type = "json"
	JSONB         Type = "jsonb"
	Date          Type = "date"
	DateTime      Type = "datetime"
	Timestamp     Type = "timestamp"
	DateTimeTZ    Type = "datetimetz"
	TimestampTZ   Type = "timestamptz"
	Time          Type = "time"
	TimeTZ        Type = "timetz"
	UUID          Type = "uuid"
	Binary        Type = "binary"
)

type Column struct {
	Name         string
	Type         Type
	collation    string
	defaultValue interface{}
	length       int
	precision    int
	total        int
	places       int
	// indexes
	index         bool
	primary       bool
	unique        bool
	useCurrent    bool
	always        bool
	autoIncrement bool
	change        bool
	nullable      bool
}

func (c *Column) Always(b bool) *Column {
	c.always = b
	return c
}

func (c *Column) IsAlways() bool {
	return c.always
}

func (c *Column) AutoIncrement() *Column {
	c.autoIncrement = true
	return c
}

func (c *Column) IsAutoIncrement() bool {
	return c.autoIncrement
}

func (c *Column) Length(l int) *Column {
	c.length = l
	return c
}

func (c *Column) GetLength(def int) int {
	if c.length == 0 {
		return def
	}

	return c.length
}

func (c *Column) Change() *Column {
	c.change = true
	return c
}

func (c *Column) Changed() bool {
	return c.change
}

func (c *Column) Collation(collation string) *Column {
	c.collation = collation
	return c
}

func (c *Column) GetCollation() string {
	return c.collation
}

func (c *Column) Default(def interface{}) *Column {
	c.defaultValue = def
	return c
}

func (c *Column) GetDefault() interface{} {
	return c.defaultValue
}

func (c *Column) SetTotal(total int) *Column {
	c.total = total
	return c
}

func (c *Column) GetTotal(def int) int {
	if c.total == 0 {
		return def
	}

	return c.total
}

func (c *Column) SetPlaces(places int) *Column {
	c.places = places
	return c
}

func (c *Column) GetPlaces(def int) int {
	if c.places == 0 {
		return def
	}

	return c.places
}

func (c *Column) SetPrecision(precision int) *Column {
	c.precision = precision
	return c
}

func (c *Column) GetPrecision() int {
	return c.precision
}

func (c *Column) Index() *Column {
	c.index = true
	return c
}

func (c *Column) Nullable() *Column {
	c.nullable = true
	return c
}

func (c *Column) IsNullable() bool {
	return c.nullable
}

func (c *Column) UseCurrent() *Column {
	c.useCurrent = true
	return c
}

func (c *Column) IsCurrent() bool {
	return c.useCurrent
}

func (c *Column) Primary() *Column {
	c.primary = true
	return c
}

func (c *Column) IsPrimary() bool {
	return c.primary
}

func (c *Column) Unique() *Column {
	c.unique = true
	return c
}

func (c *Column) IsUnique() bool {
	return c.unique
}

func (c *Column) HasIndex(index string) bool {
	switch index {
	case "primary":
		return c.primary
	case "unique":
		return c.unique
	case "index":
		return c.index
	}

	return false
}

func (c *Column) SetIndex(index string, value bool) {
	switch index {
	case "primary":
		c.primary = value
	case "unique":
		c.unique = value
	case "index":
		c.index = value
	}
}
