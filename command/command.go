package command

type TypeName string

const (
	AddCommand    TypeName = "add"
	ChangeCommand TypeName = "change"
	CreateCommand TypeName = "create"

	// index commands
	PrimaryCommand TypeName = "primary"
	UniqueCommand  TypeName = "unique"
	IndexCommand   TypeName = "index"

	// foreign
	ForeignCommand TypeName = "foreign"

	// drop commands
	DropColumnCommand  TypeName = "drop_column"
	DropPrimaryCommand TypeName = "drop_primary"
	DropUniqueCommand  TypeName = "drop_unique"
	DropIndexCommand   TypeName = "drop_index"
	DropForeignCommand TypeName = "drop_foreign"

	// rename commands
	RenameCommand      TypeName = "rename"
	RenameIndexCommand TypeName = "rename_index"
)

const (
	PrimaryLevel = 1
	IndexLevel   = 2
	RenameLevel  = 3
	DropLevel    = 4
)

// SortLevel defines how commands will sorted in execution time
var SortLevel = map[TypeName]int{
	AddCommand:    PrimaryLevel,
	ChangeCommand: PrimaryLevel,
	CreateCommand: PrimaryLevel,

	PrimaryCommand: IndexLevel,
	UniqueCommand:  IndexLevel,
	IndexCommand:   IndexLevel,
	ForeignCommand: IndexLevel,

	RenameCommand:      RenameLevel,
	RenameIndexCommand: RenameLevel,

	DropColumnCommand:  DropLevel,
	DropPrimaryCommand: DropLevel,
	DropUniqueCommand:  DropLevel,
	DropIndexCommand:   DropLevel,
	DropForeignCommand: DropLevel,
}

type Command interface {
	GetInnerType() TypeName
}
