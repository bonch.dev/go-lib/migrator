package command

// Default Command is simplified command, executing in Blueprint
type Default struct {
	InnerType TypeName
	IName     string // todo: rename
	CName     string // todo: rename
}

func (d Default) GetInnerType() TypeName {
	return d.InnerType
}

func (d *Default) GetIName() string {
	return d.IName
}

func (d *Default) GetCName() string {
	return d.CName
}
