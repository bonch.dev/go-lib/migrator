package command

type Drop struct {
	InnerType TypeName
	Name      string
	Columns   []string
}

func (d Drop) GetInnerType() TypeName {
	return d.InnerType
}

func (d *Drop) SetColumns(columns ...string) {
	d.Columns = columns
}

func (d *Drop) GetColumns() []string {
	return d.Columns
}
