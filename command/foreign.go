package command

type Foreign struct {
	InnerType          TypeName
	Name               string
	Columns            []string
	references         []string
	refOn              string
	deferrable         bool
	initiallyImmediate bool
	onDelete           string
	onUpdate           string
}

func (f Foreign) GetInnerType() TypeName {
	return f.InnerType
}

func (f *Foreign) RefColumns(columns ...string) *Foreign {
	f.references = columns
	return f
}

func (f *Foreign) GetReferences() []string {
	return f.references
}

func (f *Foreign) RefOn(tableName string) *Foreign {
	f.refOn = tableName
	return f
}

func (f *Foreign) GetRefOn() string {
	return f.refOn
}

func (f *Foreign) OnDelete(onDelete string) *Foreign {
	f.onDelete = onDelete
	return f
}

func (f *Foreign) GetOnDelete() string {
	return f.onDelete
}

func (f *Foreign) OnUpdate(onUpdate string) *Foreign {
	f.onUpdate = onUpdate
	return f
}

func (f *Foreign) GetOnUpdate() string {
	return f.onUpdate
}

func (f *Foreign) Deferrable() *Foreign {
	f.deferrable = true
	return f
}

func (f *Foreign) InitiallyImmediate() *Foreign {
	f.initiallyImmediate = true
	return f
}
