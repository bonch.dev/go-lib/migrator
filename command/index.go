package command

type Index struct {
	InnerType TypeName
	Name      string
	Columns   []string
}

func (i Index) GetInnerType() TypeName {
	return i.InnerType
}

func (i *Index) GetName() string {
	return i.Name
}

func (i *Index) GetColumns() []string {
	return i.Columns
}
