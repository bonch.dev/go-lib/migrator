module gitlab.com/bonch.dev/go-lib/migrator

go 1.21

require (
	github.com/iancoleman/strcase v0.1.2
	github.com/lib/pq v1.8.0
	github.com/mattn/go-sqlite3 v1.14.8
	github.com/pkg/errors v0.8.1
	github.com/spf13/cobra v1.1.1
)

require (
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)
