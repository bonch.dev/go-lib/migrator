package grammar

import (
	"gitlab.com/bonch.dev/go-lib/migrator/column"
	"gitlab.com/bonch.dev/go-lib/migrator/command"
)

// Modifier modify end-part of executing query.
// It can be useful to create some non integrated default logic, such as Default value of column
type Modifier interface {
	Modify(c *column.Column) string
	GetName() string
}

// Grammar is an instruction, used for compile Blueprint commands and columns to SQL code
type Grammar interface {
	CompileTableExists(schema, tableName string) string
	CompileColumnListing() string
	CompileCreate(tableName string, temporary bool, columns []*column.Column) string

	CompileAdd(tableName string, columns []*column.Column) string
	CompileDropColumn(tableName string, c *command.Drop) string

	CompilePrimary(tableName, indexName string, columns []string) string
	CompileDropPrimary(tableName string, c *command.Drop) string

	CompileIndex(tableName, indexName string, columns []string) string
	CompileDropIndex(tableName string, c *command.Drop) string

	CompileUnique(tableName, indexName string, columns []string) string
	CompileDropUnique(tableName string, c *command.Drop) string

	CompileForeign(tableName string, c *command.Foreign) string
	CompileDropForeign(tableName string, c *command.Drop) string

	ProcessColumns(columns []*column.Column) []string
	WrapTable(tableName string) string
	GetModifiers() []Modifier
	ProcessColumnType(c *column.Column) string
}
