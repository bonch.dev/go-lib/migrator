package postgres

import "strings"

func WrapValue(value string) string {
	if value != "*" {
		return "\"" + strings.ReplaceAll(value, "\"", "\"\"") + "\""
	}

	return value
}

func WrapArray(array []string) []string {
	for i := range array {
		array[i] = WrapValue(array[i])
	}

	return array
}

func PrefixArray(prefix string, array []string) (a []string) {
	for _, v := range array {
		a = append(a, prefix+" "+v)
	}

	return
}
