package postgres

import (
	"fmt"

	"gitlab.com/bonch.dev/go-lib/migrator/column"
)

type PostgreCollateModifier struct{}

func (p PostgreCollateModifier) GetName() string {
	return "collate"
}

func (p PostgreCollateModifier) Modify(c *column.Column) string {
	if c.GetCollation() != "" {
		return " collate " + WrapValue(c.GetCollation())
	}

	return ""
}

type PostgreNullableModifier struct{}

func (p PostgreNullableModifier) GetName() string {
	return "nullable"
}

func (p PostgreNullableModifier) Modify(c *column.Column) string {
	if c.IsNullable() {
		return ""
	}

	return " not null"
}

type PostgreDefaultModifier struct{}

func (p PostgreDefaultModifier) GetName() string {
	return "default"
}

func (p PostgreDefaultModifier) Modify(c *column.Column) string {
	if def := c.GetDefault(); def != nil {
		return fmt.Sprintf(" default %v", def)
	}

	return ""
}
