package postgres

import (
	"fmt"
	"strings"

	"gitlab.com/bonch.dev/go-lib/migrator/column"
	"gitlab.com/bonch.dev/go-lib/migrator/command"
	"gitlab.com/bonch.dev/go-lib/migrator/grammar"
)

type SQLGrammar struct {
	modifiers []grammar.Modifier
}

func DefaultModifiers() []grammar.Modifier {
	return []grammar.Modifier{
		new(PostgreCollateModifier),
		new(PostgreNullableModifier),
		new(PostgreDefaultModifier),
	}
}

func (g *SQLGrammar) GetModifiers() []grammar.Modifier {
	return g.modifiers
}

func (g *SQLGrammar) AddModifiers(modifiers ...grammar.Modifier) {
	for _, modifier := range modifiers {
		g.modifiers = append(g.modifiers, modifier)
	}
}

func (g SQLGrammar) CompileTableExists(schema, tableName string) string {
	return fmt.Sprintf(`
		select * 
		from information_schema.tables 
		where 
			table_schema = '%s' 
			and table_name = '%s' 
			and table_type = 'BASE TABLE'
	`, schema, tableName)
}

func (g SQLGrammar) CompileColumnListing() string {
	return `
		select column_name 
		from information_schema.columns 
		where 
			table_schema = ? 
			and table_name = ?
	`
}

func (g SQLGrammar) CompileCreate(tableName string, temporary bool, columns []*column.Column) string {
	var createExp string
	if temporary {
		createExp = "create temporary"
	} else {
		createExp = "create"
	}

	return fmt.Sprintf("%s table %s (%s)",
		createExp,
		g.WrapTable(tableName),
		strings.Join(g.ProcessColumns(columns), ", "),
	)
}

func (g SQLGrammar) CompileAdd(tableName string, columns []*column.Column) string {
	return fmt.Sprintf("alter table %s %s",
		g.WrapTable(tableName),
		strings.Join(PrefixArray("add column", g.ProcessColumns(columns)), ", "),
	)
}

func (g SQLGrammar) CompileDropColumn(tableName string, c *command.Drop) string {
	return fmt.Sprintf("alter table %s %s",
		g.WrapTable(tableName),
		strings.Join(PrefixArray("drop column", WrapArray(c.GetColumns())), ", "),
	)
}

func (g SQLGrammar) CompilePrimary(tableName, _ string, columns []string) string {
	return fmt.Sprintf("alter table %s add primary key (%s)",
		g.WrapTable(tableName),
		strings.Join(WrapArray(columns), ", "),
	)
}

func (g *SQLGrammar) CompileDropPrimary(tableName string, _ *command.Drop) string {
	return fmt.Sprintf("alter table %s drop constraint %s",
		g.WrapTable(tableName),
		WrapValue(fmt.Sprintf("%s_pkey", tableName)),
	)
}

func (g SQLGrammar) CompileIndex(tableName, indexName string, columns []string) string {
	return fmt.Sprintf("create index %s on %s (%s)",
		indexName,
		g.WrapTable(tableName),
		strings.Join(WrapArray(columns), ", "),
	)
}

func (g *SQLGrammar) CompileDropIndex(_ string, c *command.Drop) string {
	return fmt.Sprintf("drop index %s",
		g.Columnize(c.GetColumns()),
	)
}

func (g SQLGrammar) CompileUnique(tableName, indexName string, columns []string) string {
	return fmt.Sprintf("alter table %s add constraint %s unique (%s)",
		g.WrapTable(tableName),
		indexName,
		strings.Join(WrapArray(columns), ", "),
	)
}

func (g *SQLGrammar) CompileDropUnique(tableName string, c *command.Drop) string {
	return fmt.Sprintf("alter table %s drop constraint %s",
		g.WrapTable(tableName),
		g.Columnize(c.GetColumns()),
	)
}

func (g SQLGrammar) CompileForeign(tableName string, c *command.Foreign) string {
	sql := fmt.Sprintf("alter table %s add constraint %s ",
		g.WrapTable(tableName),
		c.Name,
	)

	sql += fmt.Sprintf("foreign key (%s) references %s (%s)",
		g.Columnize(c.Columns),
		g.WrapTable(c.GetRefOn()),
		g.Columnize(c.GetReferences()),
	)

	if onDelete := c.GetOnDelete(); onDelete != "" {
		sql += " on delete " + onDelete
	}

	if onUpdate := c.GetOnUpdate(); onUpdate != "" {
		sql += " on update " + onUpdate
	}

	return sql
}

func (g *SQLGrammar) CompileDropForeign(tableName string, c *command.Drop) string {
	return fmt.Sprintf("alter table %s drop constraint %s",
		g.WrapTable(tableName),
		g.Columnize(c.GetColumns()),
	)
}

func (g SQLGrammar) ProcessColumns(columns []*column.Column) []string {
	colStr := make([]string, 0)

	for _, c := range columns {
		sql := WrapValue(c.Name) + " " + g.ProcessColumnType(c)

		colStr = append(colStr, g.processModifiers(sql, c))
	}

	return colStr
}

func (g SQLGrammar) processModifiers(sql string, c *column.Column) string {
	for _, m := range g.GetModifiers() {
		sql += m.Modify(c)
	}

	return sql
}

func (g SQLGrammar) WrapTable(tableName string) string {
	return WrapValue(tableName)
}

func (g SQLGrammar) ProcessColumnType(c *column.Column) string {
	switch c.Type {
	case column.Char:
		return fmt.Sprintf("char(%d)", c.GetLength(255))
	case column.String:
		return fmt.Sprintf("varchar(%d)", c.GetLength(255))
	case column.Text, column.MediumText, column.LongText:
		return "text"
	case column.Integer, column.BigInteger, column.SmallInteger:
		return g.GeneralizableColumn(c.Type, c)
	case column.Double:
		return "double precision"
	case column.Real:
		return "real"
	case column.Decimal:
		return fmt.Sprintf("decimal(%d, %d)", c.GetTotal(10), c.GetPlaces(2))
	case column.Boolean:
		return "boolean"
	case column.JSON:
		return "json"
	case column.JSONB:
		return "jsonb"
	case column.Date:
		return "date"
	case column.DateTime, column.Timestamp:
		return fmt.Sprintf("timestamp%s without time zone%s",
			g.processPrecision(c.GetPrecision()),
			g.processCurrentTimestamp(c.IsCurrent()))
	case column.DateTimeTZ, column.TimestampTZ:
		return fmt.Sprintf("timestamp%s with time zone%s",
			g.processPrecision(c.GetPrecision()),
			g.processCurrentTimestamp(c.IsCurrent()))
	case column.Time:
		return fmt.Sprintf("time%s without time zone", g.processPrecision(c.GetPrecision()))
	case column.TimeTZ:
		return fmt.Sprintf("time%s with time zone", g.processPrecision(c.GetPrecision()))
	case column.UUID:
		return "uuid"
	case column.Binary:
		return "bytea"
	}

	return ""
}

func (g SQLGrammar) processPrecision(precision int) (formattedPrecision string) {
	if precision != 0 {
		formattedPrecision = fmt.Sprintf("(%d)", precision)
	}

	return
}

func (g SQLGrammar) processCurrentTimestamp(isCurrent bool) (formattedTimeStamp string) {
	if isCurrent {
		formattedTimeStamp = " default CURRENT_TIMESTAMP"
	}

	return
}

func (g SQLGrammar) GeneralizableColumn(t column.Type, c *column.Column) string {
	if !c.IsAutoIncrement() {
		return string(t)
	}

	switch t {
	case column.MediumInteger, column.Integer:
		return "serial"
	case column.BigInteger:
		return "bigserial"
	case column.TinyInteger, column.SmallInteger:
		return "smallserial"
	}

	return ""
}

func (g SQLGrammar) Columnize(columns []string) string {
	return strings.Join(WrapArray(columns), ", ")
}
