package modifiers

import (
	"gitlab.com/bonch.dev/go-lib/migrator/column"
)

type AutoIncrement struct{}

func (p AutoIncrement) GetName() string {
	return "auto_increment"
}

func (p AutoIncrement) Modify(_ *column.Column) string {
	return ""
}
