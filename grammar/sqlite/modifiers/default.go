package modifiers

import (
	"fmt"

	"gitlab.com/bonch.dev/go-lib/migrator/column"
)

type Default struct{}

func (p Default) GetName() string {
	return "default"
}

func (p Default) Modify(c *column.Column) string {
	if def := c.GetDefault(); def != nil {
		return fmt.Sprintf(" default %v", def)
	}

	return ""
}
