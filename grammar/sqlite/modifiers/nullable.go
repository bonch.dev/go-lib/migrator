package modifiers

import "gitlab.com/bonch.dev/go-lib/migrator/column"

type Nullable struct{}

func (p Nullable) GetName() string {
	return "nullable"
}

func (p Nullable) Modify(c *column.Column) string {
	if c.IsNullable() {
		return ""
	}

	return " NOT NULL"
}
