package modifiers

import "gitlab.com/bonch.dev/go-lib/migrator/column"

type Primary struct{}

func (p Primary) GetName() string {
	return "primary"
}

func (p Primary) Modify(c *column.Column) string {
	if c.IsPrimary() {
		switch c.Type {
		case column.Integer, column.BigInteger, column.SmallInteger, column.TinyInteger, column.MediumInteger:
			return " PRIMARY KEY AUTOINCREMENT"
		default:
			return " PRIMARY KEY"
		}
	}

	return ""
}
