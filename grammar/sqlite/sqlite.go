package sqlite

import (
	"fmt"
	"strings"

	"gitlab.com/bonch.dev/go-lib/migrator/column"
	"gitlab.com/bonch.dev/go-lib/migrator/command"
	"gitlab.com/bonch.dev/go-lib/migrator/grammar"

	sqliteModifiers "gitlab.com/bonch.dev/go-lib/migrator/grammar/sqlite/modifiers"
)

type SQLGrammar struct {
	modifiers []grammar.Modifier
}

func DefaultModifiers() []grammar.Modifier {
	return []grammar.Modifier{
		new(sqliteModifiers.Primary),
		new(sqliteModifiers.AutoIncrement),
		new(sqliteModifiers.Nullable),
		new(sqliteModifiers.Default),
	}
}

func (g *SQLGrammar) CompileTableExists(_, tableName string) string {
	return fmt.Sprintf("SELECT name FROM sqlite_master WHERE type='table' and name = '%s'", tableName)
}

func (g *SQLGrammar) CompileColumnListing() string {
	return ""
}

func (g *SQLGrammar) CompileCreate(tableName string, temporary bool, columns []*column.Column) string {
	var createExp string
	if temporary {
		createExp = "create temporary"
	} else {
		createExp = "create"
	}

	return fmt.Sprintf("%s table %s (%s)",
		createExp,
		g.WrapTable(tableName),
		strings.Join(g.ProcessColumns(columns), ", "),
	)
}

func (g *SQLGrammar) CompileAdd(tableName string, columns []*column.Column) string {
	return fmt.Sprintf("alter table %s %s",
		g.WrapTable(tableName),
		strings.Join(PrefixArray("add column", g.ProcessColumns(columns)), ", "),
	)
}

func (g *SQLGrammar) CompileDropColumn(tableName string, c *command.Drop) string {
	return fmt.Sprintf("alter table %s %s",
		g.WrapTable(tableName),
		strings.Join(PrefixArray("drop column", WrapArray(c.GetColumns())), ", "),
	)
}

func (g *SQLGrammar) CompilePrimary(_, _ string, _ []string) string {
	return ""
}

func (g *SQLGrammar) CompileDropPrimary(_ string, _ *command.Drop) string {
	return ""
}

func (g *SQLGrammar) CompileIndex(tableName, indexName string, columns []string) string {
	return fmt.Sprintf("create index %s on %s (%s)",
		indexName,
		g.WrapTable(tableName),
		strings.Join(WrapArray(columns), ", "),
	)
}

func (g *SQLGrammar) CompileDropIndex(_ string, c *command.Drop) string {
	return fmt.Sprintf("drop index %s",
		g.Columnize(c.GetColumns()),
	)
}

func (g *SQLGrammar) CompileUnique(_, _ string, _ []string) string {
	return ""
}

func (g *SQLGrammar) CompileDropUnique(_ string, _ *command.Drop) string {
	return ""
}

func (g *SQLGrammar) CompileForeign(_ string, _ *command.Foreign) string {
	return ""
}

func (g *SQLGrammar) CompileDropForeign(_ string, _ *command.Drop) string {
	return ""
}

func (g *SQLGrammar) ProcessColumns(columns []*column.Column) []string {
	colStr := make([]string, 0)

	for _, c := range columns {
		sql := WrapValue(c.Name) + " " + g.ProcessColumnType(c)

		colStr = append(colStr, g.processModifiers(sql, c))
	}

	return colStr
}

func (g SQLGrammar) processModifiers(sql string, c *column.Column) string {
	for _, m := range g.GetModifiers() {
		sql += m.Modify(c)
	}

	return sql
}

func (g *SQLGrammar) WrapTable(tableName string) string {
	return WrapValue(tableName)
}

func (g *SQLGrammar) GetModifiers() []grammar.Modifier {
	return g.modifiers
}

func (g *SQLGrammar) AddModifiers(modifiers ...grammar.Modifier) {
	for _, modifier := range modifiers {
		g.modifiers = append(g.modifiers, modifier)
	}
}

func (g SQLGrammar) ProcessColumnType(c *column.Column) string {
	switch c.Type {
	case column.Char:
		return fmt.Sprintf("char(%d)", c.GetLength(255))
	case column.String:
		return fmt.Sprintf("varchar(%d)", c.GetLength(255))
	case column.Text, column.MediumText, column.LongText:
		return "text"
	case column.Integer, column.BigInteger, column.SmallInteger:
		return "INTEGER"
	case column.Double:
		return "double precision"
	case column.Real:
		return "real"
	case column.Decimal:
		return fmt.Sprintf("decimal(%d, %d)", c.GetTotal(10), c.GetPlaces(2))
	case column.Boolean:
		return "boolean"
	case column.JSON:
		return "json"
	case column.JSONB:
		return "jsonb"
	case column.Date:
		return "date"
	case column.DateTime, column.Timestamp:
		return "datetime"
	case column.DateTimeTZ, column.TimestampTZ:
		return "datetime"
	case column.Time:
		return "datetime"
	case column.TimeTZ:
		return "datetime"
	case column.UUID:
		return "uuid"
	case column.Binary:
		return "bytea"
	}

	return ""
}

func (g SQLGrammar) processPrecision(precision int) (formattedPrecision string) {
	if precision != 0 {
		formattedPrecision = fmt.Sprintf("(%d)", precision)
	}

	return
}

func (g SQLGrammar) processCurrentTimestamp(isCurrent bool) (formattedTimeStamp string) {
	if isCurrent {
		formattedTimeStamp = " default CURRENT_TIMESTAMP"
	}

	return
}

func (g SQLGrammar) Columnize(columns []string) string {
	return strings.Join(WrapArray(columns), ", ")
}
