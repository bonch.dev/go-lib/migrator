package helper

import (
	"fmt"
	"log/slog"
	"os"
)

type slogger struct {
	log *slog.Logger
}

func (s slogger) Debugf(format string, args ...interface{}) {
	s.log.Debug(fmt.Sprintf(format, args...))
}

func (s slogger) Infof(format string, args ...interface{}) {
	s.log.Info(fmt.Sprintf(format, args...))
}

func (s slogger) Printf(format string, args ...interface{}) {
	s.log.Debug(fmt.Sprintf(format, args...))
}

func (s slogger) Warningf(format string, args ...interface{}) {
	s.log.Warn(fmt.Sprintf(format, args...))
}

func (s slogger) Errorf(format string, args ...interface{}) {
	s.log.Error(fmt.Sprintf(format, args...))
}

func (s slogger) Fatalf(format string, args ...interface{}) {
	s.log.Error(fmt.Sprintf(format, args...))
	os.Exit(1)
}

func FromSlog(logger *slog.Logger) Logger {
	return &slogger{log: logger}
}
