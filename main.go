package main

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/bonch.dev/go-lib/migrator/cmd"
)

var rootCmd = &cobra.Command{
	Use:   "migrator",
	Short: "Migrator CLI is a helper to create own migration",
	Long:  "Migrator CLI is a helper to create own migration",
}

func init() {
	rootCmd.AddCommand(cmd.VersionCommand)
	rootCmd.AddCommand(cmd.CreateCommand())
}

// migrator create -d ./db/migrations my_migration
func main() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
