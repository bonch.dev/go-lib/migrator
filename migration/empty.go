package migration

import (
	"gitlab.com/bonch.dev/go-lib/migrator/builder"
)

// Empty creates EmptyMigration with custom up function
func Empty(name string, up func(builder builder.Builder)) *EmptyMigration {
	return &EmptyMigration{
		name: name,
		up:   up,
	}
}

type EmptyMigration struct {
	BaseMigration
	up   func(builder builder.Builder)
	name string
}

func (m EmptyMigration) Name() string {
	return m.name
}

func (m EmptyMigration) Up(b builder.Builder) {
	m.up(b)
}
