package migration

import (
	"database/sql"
	"errors"

	"gitlab.com/bonch.dev/go-lib/migrator/builder"
	"gitlab.com/bonch.dev/go-lib/migrator/grammar"
	"gitlab.com/bonch.dev/go-lib/migrator/helper"
)

// Migration is atomic part of Migrator, used for creating migrations of DB
// As part of Migrator, it has a Builder interface, used for creating Blueprints.
// Also it allows make executable internal code, such as Data migration/creation.
type Migration interface {
	Up(builder builder.Builder)
	Down(builder builder.Builder)

	SetConnection(conn *sql.Conn)
	SetGrammar(grammar grammar.Grammar)
	SetLogger(logger helper.Logger)

	Name() string
	Priority() Priority
	Logger() helper.Logger
}

var (
	ErrMigrationNotImplemented = errors.New("migration method isn't implemented")
)

type BaseMigration struct {
	Connection *sql.Conn
	Grammar    grammar.Grammar
	logger     helper.Logger
}

// Up - default function to apply migration
func (b *BaseMigration) Up(_ builder.Builder) {
	panic(ErrMigrationNotImplemented)
}

// Down - default function to rollback migration
// todo: implement usage of down migrations
func (b *BaseMigration) Down(_ builder.Builder) {
	panic(ErrMigrationNotImplemented)
}

func (b *BaseMigration) SetConnection(conn *sql.Conn) {
	b.Connection = conn
}

func (b *BaseMigration) SetGrammar(g grammar.Grammar) {
	b.Grammar = g
}

func (b *BaseMigration) SetLogger(logger helper.Logger) {
	b.logger = logger
}

func (b *BaseMigration) Name() string {
	return ""
}

func (b *BaseMigration) Priority() Priority {
	return NormalPriority
}

func (b *BaseMigration) Logger() helper.Logger {
	return b.logger
}
