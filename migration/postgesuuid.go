package migration

import (
	"gitlab.com/bonch.dev/go-lib/migrator/builder"
)

type MExt0001UUID struct {
	BaseMigration
}

func (m MExt0001UUID) Name() string {
	return "MHP0001UUID"
}

func (m MExt0001UUID) Priority() Priority {
	return HighPriority
}

func (m MExt0001UUID) Up(b builder.Builder) {
	if err := b.Exec("CREATE EXTENSION IF NOT EXISTS pgcrypto SCHEMA pg_catalog"); err != nil {
		panic(err.Error())
	}
}
