package migration

type Priority int

const (
	// LowPriority is the lowest migration sorts priority.
	LowPriority Priority = iota

	// NormalPriority is usual migration sorts priority. It used in BaseMigration.
	NormalPriority

	// HighPriority used for migrations, which
	HighPriority
)
