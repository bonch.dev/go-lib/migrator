package migrator

import (
	"sort"

	"gitlab.com/bonch.dev/go-lib/migrator/migration"
)

var migrationsList []migration.Migration

func init() {
	migrationsList = make([]migration.Migration, 0)
}

// AddMigration adds a SINGLE migration to the migrations list.
func AddMigration(m migration.Migration) {
	migrationsList = append(migrationsList, m)
}

// AddMigrations adds multiple migrations to the migrations list.
func AddMigrations(ms ...migration.Migration) {
	migrationsList = append(migrationsList, ms...)
}

func sortedList() []migration.Migration {
	s := migrationsList

	sort.Slice(s, func(i, j int) bool {
		im := s[i]
		jm := s[j]

		if im.Priority() == jm.Priority() {
			return s[i].Name() < s[j].Name()
		}

		return im.Priority() > jm.Priority()
	})

	return s
}
