package migrator

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"reflect"

	"github.com/pkg/errors"

	"gitlab.com/bonch.dev/go-lib/migrator/blueprint"
	"gitlab.com/bonch.dev/go-lib/migrator/builder"
	"gitlab.com/bonch.dev/go-lib/migrator/column"
	"gitlab.com/bonch.dev/go-lib/migrator/grammar"
	"gitlab.com/bonch.dev/go-lib/migrator/helper"
	"gitlab.com/bonch.dev/go-lib/migrator/migration"
)

var (
	ErrNoConnectionSet = errors.New("no connection")
	ErrNoBatchRows     = errors.New("batch rows not found")
)

type Migrator struct {
	db            helper.DB
	grammar       grammar.Grammar
	builder       builder.Builder
	logger        helper.Logger
	runPersistent bool
}

// Init creates and initilize Migrator
// It also creates default Migration table, so use it with care
func Init(db helper.DB, g grammar.Grammar, l helper.Logger) *Migrator {
	b := builder.NewBuilder(db, g, l)

	return &Migrator{
		db:            db,
		grammar:       g,
		builder:       b,
		logger:        l,
		runPersistent: true,
	}
}

func (m *Migrator) DisablePersistent() {
	m.runPersistent = false
}

// AddMigrations - adding migration to migrations list
// Deprecated: use migrator.AddMigrations() instead
func (m *Migrator) AddMigrations(migrations ...migration.Migration) {
	migrationsList = append(migrationsList, migrations...)
}

// Migrate checks all installed Migrations, and if some of this missing - migrate it.
// There no ordering of migration creation, so carefully use for defining migrations in AddMigrations method
// All migrations in Migrate used own internal transaction, so it will be reverted, if something goes wrong
func (m *Migrator) Migrate() error {
	return m.MigrateCtx(context.Background())
}

func (m *Migrator) MigrateCtx(ctx context.Context) error {
	if m.db == nil {
		return ErrNoConnectionSet
	}

	if err := m.InitMigrationsTable(ctx); err != nil {
		return err
	}

	batch, err := m.getCurrentBatch(ctx)
	if err != nil {
		return err
	}

	completedMigrations := 0

	for _, mig := range sortedList() {
		exists, err := m.hasMigration(ctx, mig)
		if err != nil {
			return err
		}

		if !exists {
			if err := m.upMigration(ctx, mig, batch); err != nil {
				m.logger.Errorf(fmt.Sprintf("%q failed to migrate", mig.Name()))

				return err
			}

			m.logger.Infof(fmt.Sprintf("%q migrated", mig.Name()))

			completedMigrations++
		}
	}

	if completedMigrations == 0 {
		m.logger.Infof("Up-to-date; Nothing to migrate")
	}

	m.logger.Debugf("%d migrations was completed", completedMigrations)

	return nil
}

func (m *Migrator) InitMigrationsTable(ctx context.Context) error {
	exists, err := m.builder.HasTable(ctx, "migrations")
	if err != nil {
		return errors.Wrap(err, "init migration table failed")
	}

	if exists {
		return nil
	}

	m.builder.Create("migrations", func(blueprint *blueprint.Blueprint) {
		blueprint.AddColumn(column.BigInteger, "id").Primary().Index().AutoIncrement()
		blueprint.AddColumn(column.Text, "name")
		blueprint.AddColumn(column.Integer, "batch")
	})

	return nil
}

// upMigration creates transaction and exec Up method of Migration
func (m *Migrator) upMigration(ctx context.Context, mig migration.Migration, batch int) error {
	tx, err := m.db.BeginTx(ctx, &sql.TxOptions{
		Isolation: sql.LevelDefault,
		ReadOnly:  false,
	})
	if err != nil {
		return err
	}

	return func() (err error) {
		defer func() {
			if r := recover(); r != nil {
				if err := tx.Rollback(); err != nil {
					log.Fatalf("cannot rollback migrations: %s", err.Error())
				}

				err = fmt.Errorf("panic in migration: %v", r)

				return
			}

			if err := tx.Commit(); err != nil {
				log.Fatalf("cannot commit migrations: %s", err.Error())
			}

			err = m.registerMigration(ctx, mig, batch)
		}()

		m.builder.SetTransaction(tx)

		m.builder.CanRunPersistent(m.runPersistent)

		mig.SetLogger(m.logger)

		mig.Up(m.builder)

		return
	}()
}

func (m *Migrator) Wipe() error {
	return errors.Wrap(m.builder.Wipe(), "wipe failed")
}

// getMigrationName return name of migration.
// If it doesn't persist, it will use reflect to create it from used interface name
func (m *Migrator) getMigrationName(mig migration.Migration) string {
	name := mig.Name()
	if name == "" {
		name = reflect.TypeOf(mig).String()
	}

	return name
}

// hasMigration checks, that migration already persisted on database.
func (m *Migrator) hasMigration(ctx context.Context, mig migration.Migration) (bool, error) {
	rows, err := m.db.QueryContext(
		ctx,
		fmt.Sprintf("select * from migrations where name = '%s'", m.getMigrationName(mig)),
	)

	if err != nil {
		return false, err
	}

	if err := rows.Err(); err != nil {
		return false, err
	}

	defer rows.Close()

	return rows.Next(), nil
}

// registerMigration adds migration row to migrations table
func (m *Migrator) registerMigration(ctx context.Context, mig migration.Migration, batch int) error {
	_, err := m.db.ExecContext(
		ctx,
		fmt.Sprintf("insert into migrations (name, batch) values ('%s', %d)", m.getMigrationName(mig), batch),
	)

	return errors.Wrap(err, "register migration failed")
}

func (m *Migrator) getCurrentBatch(ctx context.Context) (int, error) {
	rows, err := m.db.QueryContext(
		ctx,
		m.builder.BatchSQL(),
	)
	if err != nil {
		return 0, err
	}

	defer rows.Close()

	if err := rows.Err(); err != nil {
		return 0, err
	}

	var batch int64

	if n := rows.Next(); !n {
		return 0, ErrNoBatchRows
	}

	if err := rows.Scan(&batch); err != nil {
		return 0, err
	}

	return int(batch), nil
}
