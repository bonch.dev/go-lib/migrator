package migrator_test

import (
	"fmt"
	"os"

	"gitlab.com/bonch.dev/go-lib/migrator/helper"
)

var _ helper.Logger = (*LogWriter)(nil)

type LogWriter struct {
	Trace   func(string, ...interface{})
	Debug   func(string, ...interface{})
	Info    func(string, ...interface{})
	Warning func(string, ...interface{})
	Error   func(string, ...interface{})
	Print   func(string, ...interface{})
}

func (w *LogWriter) Debugf(format string, args ...interface{}) {
	w.Debug(format, args...)
}

func (w *LogWriter) Infof(format string, args ...interface{}) {
	w.Info(format, args...)
}

func (w *LogWriter) Printf(format string, args ...interface{}) {
	w.Print(format, args...)
}

func (w *LogWriter) Warningf(format string, args ...interface{}) {
	w.Warning(format, args...)
}

func (w *LogWriter) Errorf(format string, args ...interface{}) {
	w.Error(format, args...)
}

func (w *LogWriter) Fatalf(format string, args ...interface{}) {
	w.Error(format, args...)

	os.Exit(1)
}

func DefaultLogWriter() *LogWriter {
	lw := NewLogWriter()

	lw.Trace = func(s string, i ...interface{}) {
		println(fmt.Sprintf("[trace]: %s", fmt.Sprintf(s, i...)))
	}

	lw.Debug = func(s string, i ...interface{}) {
		println(fmt.Sprintf("[debug]: %s", fmt.Sprintf(s, i...)))
	}

	lw.Info = func(s string, i ...interface{}) {
		println(fmt.Sprintf("[info]: %s", fmt.Sprintf(s, i...)))
	}

	lw.Warning = func(s string, i ...interface{}) {
		println(fmt.Sprintf("[warn]: %s", fmt.Sprintf(s, i...)))
	}

	lw.Error = func(s string, i ...interface{}) {
		println(fmt.Sprintf("[error]: %s", fmt.Sprintf(s, i...)))
	}

	return lw
}

func NewLogWriter() *LogWriter {
	return &LogWriter{}
}
