package migrator_test

import (
	"context"
	"database/sql"
	"testing"

	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/bonch.dev/go-lib/migrator/blueprint"
	"gitlab.com/bonch.dev/go-lib/migrator/builder"
	"gitlab.com/bonch.dev/go-lib/migrator/column"
	"gitlab.com/bonch.dev/go-lib/migrator/grammar/postgres"
	"gitlab.com/bonch.dev/go-lib/migrator/grammar/sqlite"
	"gitlab.com/bonch.dev/go-lib/migrator/migration"
	"gitlab.com/bonch.dev/go-lib/migrator/migrator"
)

func TestMigrator_MigratePostgres(t *testing.T) {
	db, err := sql.Open("postgres",
		"user=local password=local dbname=local sslmode=disable host=localhost port=5432",
	)
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	grammar := new(postgres.SQLGrammar)
	grammar.AddModifiers(postgres.DefaultModifiers()...)

	lw := NewLogWriter()

	mg := migrator.Init(
		db,
		grammar,
		lw,
	)

	if err := mg.Wipe(); err != nil {
		t.Fatal(err.Error())
	}

	migrator.AddMigrations(
		migration.Empty("000_idxtest_c_5", func(builder builder.Builder) {
			builder.Create("idxtest", func(blueprint *blueprint.Blueprint) {
				blueprint.AddColumn(column.BigInteger, "id").AutoIncrement()
				blueprint.AddColumn(column.BigInteger, "idx").Index()
				blueprint.AddColumn(column.Text, "idun")

				blueprint.AddUnique("idx", "idun")
				blueprint.AddIndex("id", "idx", "idun")

				blueprint.AddPrimary("id", "idx")
			})

			builder.Create("idxtest_second", func(blueprint *blueprint.Blueprint) {
				blueprint.AddColumn(column.Text, "idxtest_idun")
				blueprint.AddColumn(column.BigInteger, "idxtest_idx")
				blueprint.AddColumn(column.Text, "iname")

				blueprint.AddUnique("idxtest_idx", "idxtest_idun")
			})
		}),
		migration.Empty("002_idxtest_fk_5", func(builder builder.Builder) {
			builder.Table("idxtest_second", func(blueprint *blueprint.Blueprint) {
				blueprint.AddForeign("idxtest_idun", "idxtest_idx").RefOn("idxtest").RefColumns("idun", "idx")
			})
		}),
		migration.Empty("003_idxtest_dc_5", func(builder builder.Builder) {
			builder.Table("idxtest", func(blueprint *blueprint.Blueprint) {
				blueprint.DropPrimary()
				blueprint.DropIndex("idxtest_idx_index")
				blueprint.DropIndex("idxtest_id_idx_idun_index")
			})
		}),
		migration.Empty("004_idxtest_dt_5", func(builder builder.Builder) {
			builder.DropTableIfExists("idxtest")
		}),
	)

	if err := mg.Migrate(); err != nil {
		t.Fatal(err.Error())
	}

	mg.AddMigrations(migration.Empty("batch_test", func(builder builder.Builder) {

	}))

	if err := mg.Migrate(); err != nil {
		t.Fatal(err.Error())
	}

	mg.AddMigrations(migration.Empty("batch_test_2", func(builder builder.Builder) {

	}))

	if err := mg.Migrate(); err != nil {
		t.Fatal(err.Error())
	}
}

func TestMigrator_MigrateSQLite(t *testing.T) {
	db, err := sql.Open("sqlite3", "./db.sqlite")
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	grammar := new(sqlite.SQLGrammar)
	grammar.AddModifiers(sqlite.DefaultModifiers()...)

	lw := NewLogWriter()

	mg := migrator.Init(db, grammar, lw)

	if err := mg.Wipe(); err != nil {
		t.Fatal(err)
	}

	migrator.AddMigrations(
		migration.Empty("idxtest_c_5", func(builder builder.Builder) {
			builder.Create("idxtest", func(blueprint *blueprint.Blueprint) {
				blueprint.AddColumn(column.BigInteger, "id").AutoIncrement()
				blueprint.AddColumn(column.BigInteger, "idx").Index()
				blueprint.AddColumn(column.Text, "idun")

				blueprint.AddUnique("idx", "idun")
				blueprint.AddIndex("id", "idx", "idun")

				blueprint.AddPrimary("id", "idx")
			})

			builder.Create("idxtest_second", func(blueprint *blueprint.Blueprint) {
				blueprint.AddColumn(column.Text, "idxtest_idun")
				blueprint.AddColumn(column.BigInteger, "idxtest_idx")
				blueprint.AddColumn(column.Text, "iname")

				blueprint.AddUnique("idxtest_idx", "idxtest_idun")
			})
		}),
		migration.Empty("idxtest_fk_5", func(builder builder.Builder) {
			builder.Table("idxtest_second", func(blueprint *blueprint.Blueprint) {
				blueprint.AddForeign("idxtest_idun", "idxtest_idx").RefOn("idxtest").RefColumns("idun", "idx")
			})
		}),
		migration.Empty("idxtest_dc_5", func(builder builder.Builder) {
			builder.Table("idxtest", func(blueprint *blueprint.Blueprint) {
				blueprint.DropPrimary()
				blueprint.DropIndex("idxtest_idx_index")
				blueprint.DropIndex("idxtest_id_idx_idun_index")
			})
		}),
		migration.Empty("idxtest_dt_5", func(builder builder.Builder) {
			builder.DropTableIfExists("idxtest")

			builder.Persistent(func() {
				println("test")
			})
		}),
		migration.Empty("idxtest_dt_6", func(builder builder.Builder) {
			builder.Persistent(func() {
				println("test2")
			})
		}),
	)

	if err := mg.Migrate(); err != nil {
		t.Fatal(err.Error())
	}

	mg.AddMigrations(migration.Empty("batch_test", func(builder builder.Builder) {

	}))

	if err := mg.Migrate(); err != nil {
		t.Fatal(err.Error())
	}

	mg.AddMigrations(migration.Empty("batch_test_2", func(builder builder.Builder) {

	}))

	if err := mg.Migrate(); err != nil {
		t.Fatal(err.Error())
	}
}

func TestSchema(t *testing.T) {
	db, err := sql.Open("postgres",
		"user=lodbackend password=lodbackend dbname=lodbackend sslmode=disable host=bonch.dev",
	)
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	defer db.Close()

	grammar := new(postgres.SQLGrammar)
	grammar.AddModifiers(postgres.DefaultModifiers()...)

	lw := NewLogWriter()

	b := builder.NewBuilder(
		db,
		grammar,
		lw,
	)

	tx, err := db.BeginTx(context.Background(), &sql.TxOptions{
		Isolation: sql.LevelDefault,
		ReadOnly:  false,
	})
	if err != nil {
		t.Fatal(err.Error())
	}

	b.SetTransaction(tx)

	defer func() {
		if r := recover(); r != nil {
			t.Error(r)

			tx.Rollback()
		}
	}()

	b.Create("name", func(blueprint *blueprint.Blueprint) {
		blueprint.AddColumn(column.Integer, "id").AutoIncrement().Primary()
		blueprint.AddColumn(column.UUID, "uuid_id").Nullable().Unique()
		blueprint.AddColumn(column.String, "str").Length(100).Nullable().Unique()

		// blueprint.AddForeign()
		// blueprint.AddUnique()
	})

	tx.Commit()
}

func TestCaller(t *testing.T) {

}
